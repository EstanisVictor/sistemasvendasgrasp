package FakeDao;

import Model.Venda;

import java.util.Vector;

public class VendaDao {
    private Vector<Venda> vendasRealizadas = new Vector<>();

    public VendaDao(Vector<Venda> vendasRealizadas) {
        this.vendasRealizadas = vendasRealizadas;
    }

    public VendaDao() {
    }

    public Vector<Venda> getVendasRealizadas() {
        return vendasRealizadas;
    }

    public void setVendasRealizadas(Vector<Venda> vendasRealizadas) {
        this.vendasRealizadas = vendasRealizadas;
    }
}
