package Model;

public class Cupom {
    private int id;
    private double porcentagemDesconto;

    public Cupom() {
    }

    public Cupom(int id, double porcentagemDesconto) {
        this.id = id;
        this.porcentagemDesconto = porcentagemDesconto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPorcentagemDesconto() {
        return porcentagemDesconto;
    }

    public void setPorcentagemDesconto(double porcentagemDesconto) {
        this.porcentagemDesconto = porcentagemDesconto;
    }
}
