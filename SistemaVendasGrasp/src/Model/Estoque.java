package Model;

import java.util.HashMap;
import java.util.Map;

public class Estoque {
    private HashMap<Produto, Integer> estoque;

    public Estoque() {
        this.estoque = new HashMap<>();
    }

    public void adicionarProduto(Produto produto, int quantidade) {
        estoque.put(produto, quantidade);
    }

    public void removerProduto(Produto produto, int quantidade) {
        int quantidadeAtual = estoque.get(produto);
        estoque.put(produto, quantidadeAtual - quantidade);
    }

    public HashMap<Produto, Integer> getEstoque() {
        return estoque;
    }

    public void imprime(){
        int i = 1;
        for(Map.Entry<Produto, Integer> e : estoque.entrySet()){
            System.out.println("Produto "+i+" -> "+e.getKey().getNome()+" | qantidade ->"+e.getValue());
            i++;
        }
    }
}
