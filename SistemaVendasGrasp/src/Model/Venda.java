package Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Venda {
    HashMap<Produto, Integer> itens;
    private double precoTotal;
    private boolean temCupom;

    public Venda() {
        this.itens = new HashMap<>();
    }

    public void adicionarItem(Produto produto, int quantidade) {
        itens.put(produto, quantidade);
    }

    public void removerItem(Produto produto, int quantidade) {
        itens.remove(produto, quantidade);
    }
    public double getPrecoTotal() {
        return precoTotal;
    }

    public boolean isTemCupom() {
        return temCupom;
    }

    public void setTemCupom(boolean temCupom) {
        this.temCupom = temCupom;
    }

    public void calcularValorTotal(){
        if(itens.isEmpty()){
            System.err.println("Ainda não foi adicionado itens a venda");
        }else{
            for (Map.Entry<Produto, Integer> p : itens.entrySet()) {
                precoTotal += p.getKey().getPreco() * p.getValue();
            }
        }
    }
}
