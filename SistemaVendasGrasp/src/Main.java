import Controller.FornecedorController;
import Model.Estoque;
import Model.Fornecedor;
import Model.Produto;

public class Main {
    public static void main(String[] args) {

        FornecedorController fornecedorController = new FornecedorController();

        fornecedorController.addFornecedor(1, "Supermercado XYZ", "Rua ABC 123");
        Produto produto = new Produto(1, "Arroz", 10.0);

        fornecedorController.fornecerPordutos(1, produto, 10);

    }
}