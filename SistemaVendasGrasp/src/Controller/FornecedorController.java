package Controller;

import Model.Estoque;
import Model.Fornecedor;
import Model.Produto;

import java.util.Vector;

public class FornecedorController {
    private Estoque estoque = new Estoque();
    private Vector<Fornecedor> fornecedores = new Vector<>();

    public void addFornecedor(int id, String nome, String endereco){
        this.fornecedores.add(new Fornecedor(id, nome, endereco));
    }

    public void fornecerPordutos(int id, Produto produto, int quantidade){

        Fornecedor fornecedor = procuraFornecedor(id);

        if (fornecedor == null){
            System.err.println("Fornecedor não encontrado");
        }else{
            estoque.adicionarProduto(produto, quantidade);
            System.out.println("Produto fornecido pelo fornecedor " + fornecedor.getNome() + " adicionado ao estoque com sucesso!");
        }
    }

    private Fornecedor procuraFornecedor(int id){
        if(!this.fornecedores.isEmpty()){
            for(Fornecedor f: this.fornecedores){
                if(f.getId() == id){
                    return f;
                }
            }
        }
        return null;
    }
}
