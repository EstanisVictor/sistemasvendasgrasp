package Controller;

import Model.Cupom;
import Model.Estoque;
import Model.Produto;
import Model.Venda;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class VendaController {
    private Estoque estoque;
    private Vector<Cupom> cupoms = new Vector<>();

    public void addCupom(int id, double porcentagem){
        this.cupoms.add(new Cupom(id, porcentagem));
    }

    public Cupom procuraCupom(int id){
        for (Cupom c: this.cupoms) {
            if (c.getId() == id){
                return c;
            }
        }
        return null;
    }

    public Venda realizaVenda(HashMap<Produto, Integer> produtos, boolean temCupom, int idCupom){
        Venda venda = new Venda();
        Cupom cupom = procuraCupom(idCupom);
        for(Map.Entry<Produto, Integer> p : produtos.entrySet()){
            if(!estoque.getEstoque().containsKey(p.getKey())){
                System.err.println("Produto " + p.getKey().getNome() + " não disponível no estoque.");
                continue;
            }
            estoque.removerProduto(p.getKey(), p.getValue());
            venda.adicionarItem(p.getKey(), p.getValue());
        }

        if(temCupom){
            venda.setTemCupom(true);
            if (cupom == null){
                System.err.println("Cupom Inválido");
            }
        }
        venda.calcularValorTotal();

        System.out.println("Venda realizada com sucesso! Valor total: " + venda.getPrecoTotal());

        return venda;
    }

}
